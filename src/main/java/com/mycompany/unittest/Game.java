/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.unittest;

/**
 *
 * @author HP
 */
import java.util.Scanner;
public class Game {
    	private Table board;
	private Player x;
	private Player o;
        
        public Game() {
		o = new Player('O');
		x = new Player('X');
        }
        
        public void play() {
		while(true) {
			playOne();
		}
	}
	
	public void playOne() {
		board = new Table(x,o);
		showWelcome();
		while(true) {
			showTable();
			showTurn();
			input();
			if(board.isFinish()) {
				break;
			}
			board.switchPlayer();
}
                showTable();
		showwinner();
		showstat();
	}
	
	private void showstat() {
		System.out.println(x.getName() + " " + "W,D,L:");
		System.out.println("  "+x.getWin() + ","+ x.getDraw() + ","+ x.getLose());
		
		System.out.println(o.getName() + " " + "W,D,L:");
		System.out.println("  "+o.getWin() + ","+ o.getDraw() + ","+ o.getLose());
		
	}
        
        private void showwinner() {
		Player player = board.getWinner();
		System.out.println(player.getName()+ "  Win !!!");
	}
	
	private void showWelcome() {
		System.out.println("Welcome to XO game");
	}
	
	private void showTable() {
		char [][] table = board.getTable();
		
		System.out.println("  1 2 3");
		for(int i = 0; i<table.length; i++) {
			System.out.print((i+1));
			for(int j = 0; j<table[i].length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}
	
	private void showTurn() {
		System.out.println(board.getCurrentPlayer().getName() + " Turn");
	}
        
        private void input() {
		Scanner k = new Scanner(System.in);
		while(true) {
			try {
				System.out.print("Please input Row Col: ");
				String input = k.nextLine();
				String[] str = input.split(" ");
				if(str.length!=2) {
					System.out.println("Please input Rol col[1-3]");
					continue;
				}
				int row = Integer.parseInt(str[0])-1;
				int col = Integer.parseInt(str[1])-1;
				if(board.setTable(row,col) == false) {
					System.out.println("Table is not empty!");
					continue;
				}
				break;
				
				
			}catch(Exception e) {
				System.out.println("Please input Rol col[1-3]");
				continue;
			}
			
			
		}
		
	
	}
	
	

}

