/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.unittest.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.mycompany.unittest.Table;

/**
 *
 * @author HP
 */
public class TableUnitTest {
    
    public TableUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    public void testRow1Winner(){
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x,o);
        table.setTable(1, 1);
        table.setTable(1, 2);
        table.setTable(1, 3);
        assertEquals(true,table.checkWin());
    }
     public void testCol1Winner(){
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x,o);
        table.setTable(1, 1);
        table.setTable(2, 1);
        table.setTable(3, 1);
        assertEquals(true,table.checkWin());
    }
     public void testSwitchPlayer() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x,o);
        table.switchPlayer();
        assertEquals('x',table.getCurrentPlayer().getName());
        
    }
   
}
